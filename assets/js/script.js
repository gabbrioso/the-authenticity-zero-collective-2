/*Navbar Script*/
let navItems =  document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
// console.log(userToken)

if(!userToken){
	navItems.innerHTML = `<li class="nav-item">
							<a href="./login.html" class="nav-link hoverable">Log In</a>
						</li>`
}else{
	navItems.innerHTML = `<li class="nav-item">
							<a href="./logout.html" class="nav-link hoverable">Log Out</a>
						</li>`	
};

/*Cursor Script*/
const $bigBall = document.querySelector('.cursor__ball--big');
const $smallBall = document.querySelector('.cursor__ball--small');
const $hoverables = document.querySelectorAll('.hoverable');
const $hoverables2 = document.querySelectorAll('.hoverable2');

// Listeners
document.body.addEventListener('mousemove', onMouseMove);
for (let i = 0; i < $hoverables.length; i++) {
  $hoverables[i].addEventListener('mouseenter', onMouseHover);
  $hoverables[i].addEventListener('mouseleave', onMouseHoverOut);
}
for (let i = 0; i < $hoverables2.length; i++) {
  $hoverables2[i].addEventListener('mouseenter', onMouseHover2);
  $hoverables2[i].addEventListener('mouseleave', onMouseHoverOut2);
}



// Move the cursor
function onMouseMove(e) {
  TweenMax.to($bigBall, .4, {
    x:e.clientX,
    y:e.clientY,
  })
  TweenMax.to($smallBall, .1, {
    x:e.clientX,
    y:e.clientY,
  })
}

// Hover an element
function onMouseHover() {
  TweenMax.to($bigBall, .3, {
    scale: 4
  })
}

function onMouseHoverOut() {
  TweenMax.to($bigBall, .3, {
    scale: 1
  })
}

function onMouseHover2() {
  TweenMax.to($bigBall, .5, {
    scale: 150
  })
}
function onMouseHoverOut2() {
  TweenMax.to($bigBall, .5, {
    scale: 1
  })
}