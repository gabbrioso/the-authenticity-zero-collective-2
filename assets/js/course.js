
//window.location.search returns the query string part of the URL
//console.log(window.location.search);

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local device
let userId = localStorage.getItem("id");

let adminUser = localStorage.getItem("isAdmin");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let enrollees = document.querySelector("#enrollees");

fetch(`https://the-authenticity-zero.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if(adminUser == "false" || !adminUser){
		enrollContainer.innerHTML = `<button id="enrollButton" class="course-button btn btn-outline-danger">Enroll</button>`

		let isEnrolled = data.enrollees;
		let enrolleesIds = [];

		for (let i = 0; i < isEnrolled.length; i++) {	
			enrolleesIds.push(isEnrolled[i].userId)
		}
		let enrollButton = document.querySelector("#enrollButton")

		let userEnrolled = enrolleesIds.includes(userId);
		console.log(userEnrolled)

		if(userEnrolled === false){
			enrollButton.addEventListener("click", () => {
				fetch("https://the-authenticity-zero.herokuapp.com/api/users/enroll", {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId,
						userId: userId
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if(data === true){
						//enrollment is successful
						alert("Thank you for enrolling! See you!")
						window.location.replace("./courses.html")
					}else{
						alert("Enrollment failed")
					}
				});	
			})				
		}else if(userEnrolled === true){
			enrollButton.addEventListener("click", () => {
				alert("User is already enrolled in this course.")
				window.location.replace("./courses.html")				
			})
		}


	}else{
		enrollContainer.innerHTML = ""
		// enrollees.innerHTML = data.enrollees
		// console.log(data.enrollees)
		let enrolleesData;
		
		let fullName = []
		let fullName2 = []

		if(data.length < 1){
		enrolleesData = "No courses available.";
		}else{
			//else iterate the courses collection and display each course
			enrollees = data.enrollees
			console.log(data)
			enrolleesData = enrollees.map(user => {
				userId = user.userId

				fetch(`https://the-authenticity-zero.herokuapp.com/api/users/${userId}`,{
				})
				.then(res => res.json())
				.then(data => {
					usersName1 = data.firstName
					usersName2 = data.lastName
					// console.log(data)
					fullName = `${usersName1} ${usersName2} `

					// console.log(fullName)
					
					fullName2 = fullName2.concat(fullName)					


					// console.log(fullName2)

					let container = document.querySelector("#enrollees");
					container.innerHTML = `<b>Users Enrolled:</b><br>${fullName2}`
				})	
			})
		};
	}
	
});