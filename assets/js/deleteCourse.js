let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

let token = localStorage.getItem("token");

let userId = localStorage.getItem("id");

let isActive = document.querySelector("#isActive");

let deleteContainer = document.querySelector("#deleteContainer");



fetch(`https://the-authenticity-zero.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	
	isActive = data.isActive
	console.log(isActive)
	if(isActive === true){
		deleteCourse.innerHTML = `<h2 class="crud-text text-center my-5">Are you sure you want to archive this program?</h2>
								<button id="enrollButton" class="course-button btn btn-outline-danger"> Archive </button>`

		enrollButton.addEventListener("click", () => {

			fetch(`https://the-authenticity-zero.herokuapp.com/api/courses/${courseId}`, {
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					isActive: false
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					//enrollment is successful
					alert("The program was archived succesfully.")
					window.location.replace("./courses.html")
				}else{
					alert("Archive failed")
				}
			});
		});
	}else{
		deleteCourse.innerHTML = `<h2 class="crud-text text-center my-5">Are you sure you want to unarchive this program?</h2>
							<button id="enrollButton" class="course-button btn btn-outline-danger"> Unarchive </button>`

	enrollButton.addEventListener("click", () => {

		fetch(`https://the-authenticity-zero.herokuapp.com/api/courses/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollment is successful
				alert("The program was unarchived succesfully.")
				window.location.replace("./courses.html")
			}else{
				alert("Unarchive failed")
			}
		});
	});
	}
});