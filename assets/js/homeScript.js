AOS.init({
  once: true,
});

/*Cursor Script*/
const $bigBall = document.querySelector('.cursor__ball--big');
const $smallBall = document.querySelector('.cursor__ball--small');
const $hoverables = document.querySelectorAll('.hoverable');
const $hoverables2 = document.querySelectorAll('.hoverable2');

// Listeners
document.body.addEventListener('mousemove', onMouseMove);
for (let i = 0; i < $hoverables.length; i++) {
  $hoverables[i].addEventListener('mouseenter', onMouseHover);
  $hoverables[i].addEventListener('mouseleave', onMouseHoverOut);
}
for (let i = 0; i < $hoverables2.length; i++) {
  $hoverables2[i].addEventListener('mouseenter', onMouseHover2);
  $hoverables2[i].addEventListener('mouseleave', onMouseHoverOut2);
}



// Move the cursor
function onMouseMove(e) {
  TweenMax.to($bigBall, .4, {
    x:e.clientX,
    y:e.clientY,
  })
  TweenMax.to($smallBall, .1, {
    x:e.clientX,
    y:e.clientY,
  })
}

// Hover an element
function onMouseHover() {
  TweenMax.to($bigBall, .3, {
    scale: 4
  })
}

function onMouseHoverOut() {
  TweenMax.to($bigBall, .3, {
    scale: 1
  })
}

function onMouseHover2() {
  TweenMax.to($bigBall, .5, {
    scale: 150
  })
}
function onMouseHoverOut2() {
  TweenMax.to($bigBall, .5, {
    scale: 1
  })
}

/*For Background Change Script*/

let programs = document.querySelector("#programs")
let programsText = document.querySelector(".programs-text")
let podcasts = document.querySelector("#podcasts")
let about = document.querySelector("#about")




function changeBG(){

  // Determine Scroll Percentage

  let winheight = window.innerHeight

  function getDocHeight() {
      var D = document;
      return Math.max(
          D.body.scrollHeight, D.documentElement.scrollHeight,
          D.body.offsetHeight, D.documentElement.offsetHeight,
          D.body.clientHeight, D.documentElement.clientHeight
      )
  }
   
  var docheight = getDocHeight()

  function amountScrolled(){
      var winheight= window.innerHeight || (document.documentElement || document.body).clientHeight
      var docheight = getDocHeight()
      var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop
      var trackLength = docheight - winheight
      var pctScrolled = Math.floor(scrollTop/trackLength * 100) // gets percentage scrolled (ie: 80 or NaN if tracklength == 0)
      return pctScrolled
  }
   
  let amountScroll = amountScrolled()

  //For Large Screens

  if (amountScroll >= 61 && amountScroll >= 75 && screen.width >= 1200) { 
      about.setAttribute('style', 'background-color:#d9534f !important');
      about.style.color = "white";
      about.style.transitionDuration = "1s";
        programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";      
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
  }else if (amountScroll >= 61 && amountScroll <= 75 && screen.width >= 1200){
    programs.setAttribute('style', 'background-color:black !important');
      programs.style.color = "white";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "white";
      podcasts.setAttribute('style', 'background-color:black !important');
      podcasts.style.color = "white";
      podcasts.style.transitionDuration = "1s";
      about.setAttribute('style', 'background-color:white !important');
      about.style.color = "black";
      about.style.transitionDuration = "1s";
    }else if (amountScroll >= 60 && screen.width >= 1200) {
      programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
    }



    // //For Medium Screens
    if (amountScroll >= 71 && amountScroll >= 82 && screen.width <= 780) { 
      about.setAttribute('style', 'background-color:#d9534f !important');
      about.style.color = "white";
      about.style.transitionDuration = "1s";
        programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";      
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
  }else if (amountScroll >= 71 && amountScroll <= 82 && screen.width <= 780){
    programs.setAttribute('style', 'background-color:black !important');
      programs.style.color = "white";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "white";
      podcasts.setAttribute('style', 'background-color:black !important');
      podcasts.style.color = "white";
      podcasts.style.transitionDuration = "1s";
      about.setAttribute('style', 'background-color:white !important');
      about.style.color = "black";
      about.style.transitionDuration = "1s";
    }else if (amountScroll >= 70 && screen.width <= 780) {
      programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
    }
    
    // //Form Small Screens
    if (amountScroll >= 54 && amountScroll >= 72 && screen.width <= 400) { 
      about.setAttribute('style', 'background-color:#d9534f !important');
      about.style.color = "white";
      about.style.transitionDuration = "1s";
        programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";      
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
  }else if (amountScroll >= 54 && amountScroll <= 72 && screen.width <= 400){
    programs.setAttribute('style', 'background-color:black !important');
      programs.style.color = "white";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "white";
      podcasts.setAttribute('style', 'background-color:black !important');
      podcasts.style.color = "white";
      podcasts.style.transitionDuration = "1s";
      about.setAttribute('style', 'background-color:white !important');
      about.style.color = "black";
      about.style.transitionDuration = "1s";
    }else if (amountScroll >= 53 && screen.width <= 400) {
      programs.setAttribute('style', 'background-color:white !important');
      programs.style.color = "black";
      programs.style.transitionDuration = "1s";
      programsText.style.color = "black";
      podcasts.setAttribute('style', 'background-color:white !important');
      podcasts.style.color = "black";
      podcasts.style.transitionDuration = "1s";
    }
}


window.onscroll = function() {changeBG()};
