let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault();

	//get the value of #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName").value;
	//get the value of #courseDescription and assign it to the description variable
	let description = document.querySelector("#courseDescription").value;
	//get the value of #price and assign it to the price variable
	let price = document.querySelector("#coursePrice").value;
	//retrieve the JSON Web Token stored in our local storage for authentication
	let token = localStorage.getItem('token');

	fetch("https://the-authenticity-zero.herokuapp.com/api/courses", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(data === true){
			//if creation of new course is successful, redirect to courses page
			window.location.replace("./courses.html")
		}else{
			alert("Error in creating course.")
		}
	})
});

/*Cursor Script*/
const $bigBall = document.querySelector('.cursor__ball--big');
const $smallBall = document.querySelector('.cursor__ball--small');
const $hoverables = document.querySelectorAll('.hoverable');
const $hoverables2 = document.querySelectorAll('.hoverable2');

// Listeners
document.body.addEventListener('mousemove', onMouseMove);
for (let i = 0; i < $hoverables.length; i++) {
  $hoverables[i].addEventListener('mouseenter', onMouseHover);
  $hoverables[i].addEventListener('mouseleave', onMouseHoverOut);
}
for (let i = 0; i < $hoverables2.length; i++) {
  $hoverables2[i].addEventListener('mouseenter', onMouseHover2);
  $hoverables2[i].addEventListener('mouseleave', onMouseHoverOut2);
}



// Move the cursor
function onMouseMove(e) {
  TweenMax.to($bigBall, .4, {
    x:e.clientX,
    y:e.clientY,
  })
  TweenMax.to($smallBall, .1, {
    x:e.clientX,
    y:e.clientY,
  })
}

// Hover an element
function onMouseHover() {
  TweenMax.to($bigBall, .3, {
    scale: 4
  })
}

function onMouseHoverOut() {
  TweenMax.to($bigBall, .3, {
    scale: 1
  })
}

function onMouseHover2() {
  TweenMax.to($bigBall, .5, {
    scale: 150
  })
}
function onMouseHoverOut2() {
  TweenMax.to($bigBall, .5, {
    scale: 1
  })
}