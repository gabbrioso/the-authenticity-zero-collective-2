let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter;

//if statement for admin users
if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML =  `<div class="col-md-3">
	<a href="./addCourse.html" class="btn btn-block btn-outline-danger login-button text-center">Add Course</a>
	</div>`
}

//fetch the courses from our API
fetch("https://the-authenticity-zero.herokuapp.com/api/courses")
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1 display "No courses available."
	if(data.length < 1){
		courseData = "No courses available.";
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			isActive = course.isActive
			if(adminUser === "false" && isActive === true){
				console.log(isActive)
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="login-button btn btn-outline-danger btn-block editButton">View Program</a>`

				return (
				`<div class="col-md-12 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-center">${course.name}</h5>
							
							<p class="card-text text-center">Price: ₱${course.price}</p>
						</div>

						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>`
				// <p class="card-text text-center">${course.description}</p>
				)	
			}else if (adminUser === "true"){
				//if user is an admin, display the edit course button
				cardFooter = `<p class="course-status card-text text-center">isActive: <b>${course.isActive}</b>
				<a href="./course.html?courseId=${course._id}" value=${course._id} class="login-button btn btn-outline-danger btn-block editButton">View Program</a>
				
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="login-button btn btn-outline-danger btn-block editButton">Edit</a>

				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="login-button btn btn-outline-danger btn-block editButton">Archive/Unarchive</a>`

				return (
				`<div class="col-md-12 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-center">${course.name}</h5>
							<p class="card-text text-center">Price: ₱${course.price}</p>
						</div>

						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>`
				)	
			}
				
					
		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. 
		//We replaced the comma with empty strings to remove them.
	};

	let container = document.querySelector("#coursesContainer");
	container.innerHTML = courseData

});